#!/bin/bash

# get and set the 
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp ./vimrc ~/.vim/vimrc
ln ~/.vimrc ~/.vim/vimrc
pushd ~/.vim/bundles/
./install.py
popd
vim +PluginInstall +qall

echo -e "Finised setting up vim!"
echo -e "vim for Python: `git remote get-url origin`"